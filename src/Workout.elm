module Workout exposing
    ( Workout(..)
    , counter
    , forDifficulty
    , name
    )

import Workout.Counter exposing (Counter)
import Workout.Difficulty exposing (Difficulty(..))


type Workout
    = Workout String Counter Int Int Int


counter : Workout -> Counter
counter (Workout _ c _ _ _) =
    c


forDifficulty : Difficulty -> Workout -> Int
forDifficulty difficulty (Workout _ _ easy medium hard) =
    case difficulty of
        Easy ->
            easy

        Medium ->
            medium

        Hard ->
            hard


name : Workout -> String
name (Workout n _ _ _ _) =
    n
