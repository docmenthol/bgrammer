port module Main exposing (main)

import Browser exposing (Document)
import Html exposing (Html, audio, button, div, h1, hr, source, text)
import Html.Attributes exposing (class, id, src, type_)
import Html.Events exposing (onClick)
import Random
import Time
import Workout as W exposing (Workout(..))
import Workout.Counter exposing (Counter(..))
import Workout.Difficulty exposing (Difficulty(..))


type Mode
    = Cooldown
    | WorkingOut
    | Idle


type alias Model =
    { timerRunning : Bool
    , timer : Float
    , reps : Int
    , workout : Maybe Workout
    , difficulty : Difficulty
    , mode : Mode
    }


type Msg
    = SetWorkout Workout
    | SetDifficulty Difficulty
    | StartWorkout
    | RandomWorkout Int
    | Tick Time.Posix


workouts : List Workout
workouts =
    [ Workout "Buttkicks" Timed 60 160 240
    , Workout "Plank" Timed 30 60 90
    , Workout "High Knees" Timed 40 60 80
    , Workout "Pullups" Reps 3 8 12
    , Workout "Pushups" Reps 5 15 25
    , Workout "Crunches" Reps 10 20 30
    , Workout "Calf Raises" Reps 5 15 25
    , Workout "Burpees" Reps 5 10 14
    , Workout "Flutter Kicks" Timed 30 45 60
    , Workout "Lunges" Reps 20 40 60
    , Workout "Squats" Reps 10 15 20
    , Workout "Steps" Reps 60 80 100
    , Workout "Bicycle Crunches" Reps 10 30 50
    ]


port playSound : String -> Cmd msg


random : Random.Generator Int
random =
    Random.int 1 <| List.length workouts


startWorkout : Model -> Workout -> Model
startWorkout model w =
    let
        counter =
            W.forDifficulty model.difficulty w
    in
    case W.counter w of
        Timed ->
            { model | timer = toFloat counter, reps = 0 }

        Reps ->
            { model | reps = counter, timer = 0.0 }


init : () -> ( Model, Cmd Msg )
init _ =
    ( { timerRunning = False, timer = 0.0, reps = 0, workout = Nothing, difficulty = Easy, mode = Idle }
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SetDifficulty d ->
            let
                newModel =
                    case model.workout of
                        Just w ->
                            startWorkout model w

                        Nothing ->
                            model
            in
            ( { newModel | timerRunning = False, difficulty = d }, Cmd.none )

        SetWorkout w ->
            let
                newModel =
                    startWorkout model w
            in
            ( { newModel | timerRunning = False, workout = Just w }, Cmd.none )

        StartWorkout ->
            ( { model | timerRunning = True }, playSound "workout-start" )

        RandomWorkout index ->
            let
                workout =
                    List.drop index workouts |> List.head

                newModel =
                    case workout of
                        Just w ->
                            startWorkout model w

                        Nothing ->
                            model
            in
            ( newModel, Cmd.none )

        Tick t ->
            -- I hate this but I dunno how else to do it.
            case model.mode of
                Cooldown ->
                    if model.timer - 0.1 > 0 then
                        ( { model | timer = model.timer - 0.1 }, Cmd.none )

                    else
                        -- Select workout here as Cmd Msg eventually.
                        ( { model | timer = 0.0 }, Cmd.none )

                WorkingOut ->
                    case ( model.workout, model.timerRunning ) of
                        ( Just workout, True ) ->
                            case W.counter workout of
                                Timed ->
                                    if model.timer - 0.1 > 0 then
                                        ( { model | timer = model.timer - 0.1 }, Cmd.none )

                                    else
                                        ( { model | timer = 0.0, timerRunning = False }, playSound "workout-stop" )

                                Reps ->
                                    ( { model | timer = model.timer + 0.1 }, Cmd.none )

                        ( _, _ ) ->
                            ( model, Cmd.none )

                Idle ->
                    ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    if model.timerRunning then
        Time.every 10 Tick

    else
        Sub.none


view : Model -> Document Msg
view _ =
    { title = "bgrammer"
    , body =
        [ div [] [ button [ class "btn bg-primary", onClick <| SetWorkout <| Workout "Pushups" Reps 5 15 25 ] [ text "Test Button" ] ]
        , audio [ id "workout-start" ] [ source [ src "sounds/start.wav", type_ "audio/wav" ] [] ]
        , audio [ id "workout-stop" ] [ source [ src "sounds/stop.wav", type_ "audio/wav" ] [] ]
        ]
    }


main : Program () Model Msg
main =
    Browser.document
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
