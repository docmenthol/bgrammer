module Workout.Difficulty exposing (Difficulty(..))


type Difficulty
    = Easy
    | Medium
    | Hard
