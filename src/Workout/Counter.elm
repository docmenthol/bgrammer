module Workout.Counter exposing (Counter(..))


type Counter
    = Timed
    | Reps
