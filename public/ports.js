app.ports.playSound.subscribe(name => {
  const sound = document.getElementById(name)
  if (sound) {
    sound.play()
  } else {
    console.error(`[Audio] Could not play sound "${name}"`)
  }
})

app.ports.saveSettings.subscribe(settings =>
  window.localStorage.setItem('settings', JSON.stringify(settings)))

app.ports.loadSettings.subscribe(() => {
  app.ports.loadSettings.send(window.localStorage.getItem('settings'))
})
