# bgrammer

A simplified clone of Brogrammer.

## TODO:

* ~~Reps support with count-up timer~~
* Settings
* Better mobile styling (smaller text, etc.)
* ~~Styling~~
* `localStorage`-backed settings
* Switch work day length to start/end times
* "Catch up" mode
* "Gains" mode
* Hydration reminder shouldn't halt timer

### "Catch up" mode

When enabled, this mode will catch you up at the start of a day if you start
late. You are immediately given an exercise and then, based on your configured
start time and workout interval, your cooldown will be set to the remaining
time as though you had started at your normal time. For example:

* Your start time is 9:00AM.
* Your interval is 1 hour.
* You start at 9:45AM.
* You get an exercise to start.
* You get your next exercise in 15 minutes.


### "Gains" mode

When enabled, the amount of time you've been using the app will be taken into
account when giving reps and times. For every few days used, an additional
couple reps or few seconds will be added to the counters. Weekends can be
accounted for.
